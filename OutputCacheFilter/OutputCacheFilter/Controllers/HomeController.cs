﻿
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OutputCacheFilter.Controllers
{
    public class HomeController : Controller
    {
       
        // GET: Home
        //ResultFilter
        [OutputCache(Duration = 15, Location = System.Web.UI.OutputCacheLocation.Server)] //duration in second and here we store the data into outputcache till 15 sec.
       // [OutputCache(Duration = 15, Location = System.Web.UI.OutputCacheLocation.Client)] // here we use two properties (Duration & Location).When we store data in client location the data will be lost each refreshing page.
        public ActionResult Index()
        {
            ViewBag.time = DateTime.Now.ToLongTimeString();
            return View();
        }
        
        public ActionResult Student()
        {
            ViewBag.time = DateTime.Now.ToLongTimeString();
            return View();
        }

    }
}