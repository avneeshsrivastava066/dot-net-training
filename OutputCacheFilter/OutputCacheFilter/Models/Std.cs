﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OutputCacheFilter.Models
{
    public class Std
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public int Age { get; set; }
    }
}