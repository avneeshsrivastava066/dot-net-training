﻿using EntityFrameWorkCoreProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EntityFrameWorkCoreProject.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeDBContex obj = new EmployeeDBContex();

        public object EmpTbls { get; private set; }
        public IActionResult Index()
        {
            var x = obj.EmpTable.ToList();
            return View(x);
        }
        public IActionResult Create()
        {
            return View();
        }


        public ActionResult Details(int id)
        {
            var Emp = obj.EmpTable.Find(id);
            if (Emp == null)
            {
                return HttpNotFound();
            }
            return View(Emp);
        }

        private ActionResult HttpNotFound()
        {
            throw new NotImplementedException("Invalid Request");
        }

        [HttpPost]
        public ActionResult Create(EmployeeModel Emp)
        {
            if (ModelState.IsValid)
            {
                obj.EmpTable.Add(Emp);
                obj.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Emp);
        }


        // GET: /Student/Edit/5
        public ActionResult Edit(int id)
        {
            var Employee = obj.EmpTable.Find(id);
            if (Employee == null)
            {
                return HttpNotFound();
            }
            return View(Employee);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeModel emp)
        {
            if (ModelState.IsValid)
            {
                obj.Entry(emp).State = EntityState.Modified;
                obj.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emp);
        }


        public ActionResult Delete(int id)
        {
            var employee = obj.EmpTable.Find(id);
            var x = obj.EmpTable.Where(a => a.EId == id).Select(y => y.EId).FirstOrDefault();

            if (employee == null)
            {
                return View();
            }

            else
            {
                obj.EmpTable.Remove(employee);
                obj.SaveChanges();
                return RedirectToAction("Index");
            }

        }
    }
}
