﻿using EntityFrameWorkCoreProject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EntityFrameWorkCoreProject
{
    public class EmployeeDBContex: DbContext
    {
        public EmployeeDBContex()
        {
        }

        public EmployeeDBContex(DbContextOptions<EmployeeDBContex> options) : base(options)
        {

        }
        public DbSet<EmployeeModel> EmpTable { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"server=AVNEESH\SQLEXPRESS2019;Database=EFCoreDB;User Id=sa;Password=avneesh@123");
        }
       
    }
}
