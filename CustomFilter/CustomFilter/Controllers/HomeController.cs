﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomFilter.Models;

namespace CustomFilter.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [LogFilter]
        public ActionResult Index()
        {
            return View();
        }


        [LogCustomExceptionFilter]
        public ActionResult Contact()
        {
            throw new NullReferenceException();
            return View();
        }
    }
}