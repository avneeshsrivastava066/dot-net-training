﻿using CrudOperationMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrudOperationMVC.Repository;

namespace CrudOperationMVC.Controllers
{
    public class EmployeeController : Controller
    {
        DataAccessLayer dal = new DataAccessLayer();
       // GET: Employee
       [HttpGet]
        public ActionResult Index ()
        {
           ModelState.Clear();
          return View(dal.GetDetail());
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View(dal.GetDetail().Find(emp=>emp.EId==Convert.ToInt32(id)));
        }

        // GET: Employee/Create

        [HttpGet]
        public ActionResult Create()
        {
            return View(new EmployeeModel());
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeModel ur)
        {
            try
            {
                if (dal.InsertData(ur))
                {
                    ViewBag.Message = "Data Saved";
                }

                return RedirectToAction("Index");
            }

            catch
            {
                return View();
            }


             
            
           

        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            EmployeeModel objModel = new EmployeeModel();

            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
