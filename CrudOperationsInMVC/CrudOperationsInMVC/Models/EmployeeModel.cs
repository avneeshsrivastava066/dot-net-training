﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CrudOperationMVC.Models
{
    public class EmployeeModel
    {
        public int EId { get; set; }
        [DisplayName("Employee Name")]
        public string EmpName { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
    }
}