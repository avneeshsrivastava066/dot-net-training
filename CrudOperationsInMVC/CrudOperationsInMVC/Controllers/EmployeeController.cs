﻿using CrudOperationMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrudOperationMVC.Repository;

namespace CrudOperationMVC.Controllers
{
    public class EmployeeController : Controller
    {
        DataAccessLayer dal = new DataAccessLayer();
        // GET: Employee
        [HttpGet]
        public ActionResult Index()
        {
            ModelState.Clear();
            return View(dal.GetDetail());
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View(dal.GetDetail().Find(emp => emp.EId == id));
        }

        // GET: Employee/Create

        [HttpGet]
        public ActionResult Create()
        {
            return View(new EmployeeModel());
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeModel ur)
        {
            dal.InsertData(ur);
            return RedirectToAction("Index");
            //try
            //{
            //    if (dal.InsertData(ur))
            //    {
            //        ViewBag.Message = "Data Saved";
            //    }

            //    return RedirectToAction("Index");
            //}

            //catch
            //{
            //    return View();
            //}






        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dal.GetDetail().Find(emp => emp.EId == id));
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EmployeeModel ur)
        {
            dal.UpdateData(ur);
            return RedirectToAction("Index");
            //try
            //{
            //    // TODO: Add update logic here

            //    if (dal.UpdateData(ur))
            //    {
            //        ViewBag.Message = "Data Update";
            //    }

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            dal.DeleteData(id);
            return RedirectToAction("Index");
            //try
            //{
            //    // TODO: Add delete logic here

            //    if (dal.DeleteData(EId))
            //    {
            //        ViewBag.Message = "Data Deleted";
            //    }

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        // POST: Employee/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, EmployeeModel ur)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        if (dal.DeleteData(ur))
        //        {
        //            ViewBag.Message = "Data Deleted";
        //        }

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
