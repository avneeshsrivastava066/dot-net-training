﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using CrudOperationMVC.Models;
using System.Data;

namespace CrudOperationMVC.Repository
{
    public class DataAccessLayer
    {
        // create a connection object with connection string
        string cs = "Data Source=AVNEESH\\SQLEXPRESS2019;Initial Catalog=MvcDb;User Id=sa;Password=avneesh@123";

        public List<EmployeeModel> GetDetail()
        {
            DataTable dtblEmployee = new DataTable();
            List<EmployeeModel> lst = new List<EmployeeModel>();
            using (SqlConnection sqlCon = new SqlConnection(cs))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM EmpTbl", sqlCon);
                sqlDa.Fill(dtblEmployee);
                foreach (DataRow dr in dtblEmployee.Rows)
                {
                    lst.Add(new EmployeeModel
                    {
                        EId = Convert.ToInt32(dr[0]),
                        EmpName = Convert.ToString(dr[1]),
                        EmailId = Convert.ToString(dr[2]),
                        MobileNo = Convert.ToString(dr[3]),
                    });
                }
            }
            return lst;
        }


        public void InsertData(EmployeeModel ur)
        {
            //int i;
            SqlConnection sqlCon = new SqlConnection(cs);
            string query = "INSERT INTO EmpTbl VALUES(@EmpName,@EmailId,@MobileNo)";
            SqlCommand sqlcmd = new SqlCommand(query, sqlCon);
            sqlcmd.Parameters.AddWithValue("@EmpName", ur.EmpName);
            sqlcmd.Parameters.AddWithValue("@EmailId", ur.EmailId);
            sqlcmd.Parameters.AddWithValue("@MobileNo", ur.MobileNo);
            sqlCon.Open();
            sqlcmd.ExecuteNonQuery();
            sqlCon.Close();
            //if (i >= 1)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}


        }


        public void UpdateData(EmployeeModel ur)
        {
            //int i;
            SqlConnection sqlCon = new SqlConnection(cs);
            //string query = 

            SqlCommand sqlcmd = new SqlCommand("UPDATE EmpTbl SET EmpName = @EmpName, EmailId = @EmailId, MobileNo = @MobileNo WHERE EId = @EId ", sqlCon);
            sqlcmd.Parameters.AddWithValue("@EId", ur.EId);
            sqlcmd.Parameters.AddWithValue("@EmpName", ur.EmpName);
            sqlcmd.Parameters.AddWithValue("@EmailId", ur.EmailId);
            sqlcmd.Parameters.AddWithValue("@MobileNo", ur.MobileNo);

            sqlCon.Open();
            sqlcmd.ExecuteNonQuery();
            sqlCon.Close();
            //if (i >= 1)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}


        }


        public void DeleteData(int EId)
        {
           
            SqlConnection sqlCon = new SqlConnection(cs);
            SqlCommand sqlcmd = new SqlCommand("DELETE FROM EmpTbl WHERE EId=@EId", sqlCon);
            sqlcmd.Parameters.AddWithValue("@EId", EId);
            sqlCon.Open();
           sqlcmd.ExecuteNonQuery();
            sqlCon.Close();
            //if (i >= 1)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}


        }





    }
}