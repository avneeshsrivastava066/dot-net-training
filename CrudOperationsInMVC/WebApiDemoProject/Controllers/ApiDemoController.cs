﻿using CrudOperationMVC.Models;
using CrudOperationMVC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiDemoProject.Controllers
{
    public class ApiDemoController : ApiController
    {
        DataAccessLayer dal = new DataAccessLayer();

        public IHttpActionResult Getrecord()
        {
            return Ok(dal.GetDetail());
        }

        public IHttpActionResult InsertUser(EmployeeModel ur)
        {
            dal.InsertData(ur);
            return Ok();
        }


        public IHttpActionResult PutUserUpdate(EmployeeModel ur)
        {
            dal.UpdateData(ur);
            return Ok();
        }
        
        public IHttpActionResult DeleteUser(int id)
        {
            dal.DeleteData(id);
            return Ok();
        }



    }
}
