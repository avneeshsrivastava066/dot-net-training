﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApiDemoProject.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            IEnumerable<EmployeeModel> employee = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/");
                var responseTask = client.GetAsync("Employee");
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readJob = result.Content.ReadAsAsync<IList<EmployeeModel>>();
                    readJob.Wait();
                    Employee = readJob.Result;
                }
                else
                {
                    //return the error code here
                    Employee = Enumerable.Empty<EmployeeModel>();
                    ModelState.AddModelError(string.Empty, "Error occured !");
                }

            }
            return View();
        }
        public ActionResult Create()
        {
            return View();

        }
        [HttpPost]

        public ActionResult Create(EmployeeModel emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/Student");
                var postTask = client.PostAsJsonAsync<EmployeeModel>("Employee", Emp);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View();
            }
        }
        public ActionResult Edit(int id)
        {
            EmployeeModel model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/");
                var responseTask = client.GetAsync("Employee/" + id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<EmployeeModel>();
                    readTask.Wait();
                    model = readTask.Result;

                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(EmployeeModel employee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/Student");
                var putTask = client.PutAsJsonAsync<StudentModel>("student", employee);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(student);
            }
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:62516/api/");
                var deleteTask = client.DeleteAsync("Employee/" + id.ToString());
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }

    }
}