﻿using CRUDOprationAsp.NetCore.EmployeeDB;
using CRUDOprationAsp.NetCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApiDemoAspdotNetCore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        //[HttpGet]
        //public IEnumerable<WeatherForecast> Get()
        //{
        //    var rng = new Random();
        //    return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    {
        //        Date = DateTime.Now.AddDays(index),
        //        TemperatureC = rng.Next(-20, 55),
        //        Summary = Summaries[rng.Next(Summaries.Length)]
        //    })
        //    .ToArray();
        //}


        DataAccessLayer dal = new DataAccessLayer();

        [HttpGet]
        public IActionResult GetAllEmployee()
        {
            return Ok(dal.GetEmpList());
        }

        [HttpGet("{id}")]
        public IActionResult GetEditSudent(int id)
        {
            var getData = dal.GetEditById(id);

            return Ok(getData);
        }


        [HttpPost]
        public IActionResult PostEmployee(EmployeeModel model)
        {
            dal.InsertEmployee(model);
            return Ok();
        }




        [HttpPut]
        public IActionResult PutUpadateEmployee(EmployeeModel model)
        {
            dal.UpdateData(model);
            return Ok();
        }
        [HttpDelete]
        public IActionResult DeleteStudent(int id)
        {
            dal.DeleteEmployee(id);
            return Ok();
        }

    }
}
