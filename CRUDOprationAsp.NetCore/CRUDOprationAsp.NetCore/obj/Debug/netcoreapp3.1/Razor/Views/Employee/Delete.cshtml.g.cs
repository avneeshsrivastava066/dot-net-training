#pragma checksum "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "12904e9abe950a935eabda67ee1d22b9d1147ae7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Employee_Delete), @"mvc.1.0.view", @"/Views/Employee/Delete.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\_ViewImports.cshtml"
using CRUDOprationAsp.NetCore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\_ViewImports.cshtml"
using CRUDOprationAsp.NetCore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"12904e9abe950a935eabda67ee1d22b9d1147ae7", @"/Views/Employee/Delete.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"001c302b8d97d650684093a094b7460cece0feb8", @"/Views/_ViewImports.cshtml")]
    public class Views_Employee_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CRUDOprationAsp.NetCore.Models.EmployeeModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
  
    ViewData["Title"] = "Delete";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h2>Delete</h2>\r\n\r\n<h3>Are you sure you want to delete this?</h3>\r\n<div>\r\n    <h4>EmployeeModel</h4>\r\n    <hr />\r\n    <dl class=\"dl-horizontal\">\r\n        <dt>\r\n            ");
#nullable restore
#line 15 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.EId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n\r\n        <dd>\r\n            ");
#nullable restore
#line 19 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayFor(model => model.EId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n\r\n        <dt>\r\n            ");
#nullable restore
#line 23 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.EmpName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n\r\n        <dd>\r\n            ");
#nullable restore
#line 27 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayFor(model => model.EmpName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n\r\n        <dt>\r\n            ");
#nullable restore
#line 31 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.EmailId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n\r\n        <dd>\r\n            ");
#nullable restore
#line 35 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayFor(model => model.EmailId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n\r\n        <dt>\r\n            ");
#nullable restore
#line 39 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MobileNo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n\r\n        <dd>\r\n            ");
#nullable restore
#line 43 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MobileNo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n\r\n    </dl>\r\n\r\n");
#nullable restore
#line 48 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
     using (Html.BeginForm())
    {
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 50 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
   Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"form-actions no-color\">\r\n            <input type=\"submit\" value=\"Delete\" class=\"btn btn-default\" /> |\r\n            ");
#nullable restore
#line 54 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
       Write(Html.ActionLink("Back to List", "Index"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n");
#nullable restore
#line 56 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Delete.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CRUDOprationAsp.NetCore.Models.EmployeeModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
