#pragma checksum "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a45fbf1b2680aaac2733b1c3137e3162c78ebe9f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Employee_Index), @"mvc.1.0.view", @"/Views/Employee/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\_ViewImports.cshtml"
using CRUDOprationAsp.NetCore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\_ViewImports.cshtml"
using CRUDOprationAsp.NetCore.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a45fbf1b2680aaac2733b1c3137e3162c78ebe9f", @"/Views/Employee/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"001c302b8d97d650684093a094b7460cece0feb8", @"/Views/_ViewImports.cshtml")]
    public class Views_Employee_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<CRUDOprationAsp.NetCore.Models.EmployeeModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Index</h1>\r\n\r\n<p>\r\n    ");
#nullable restore
#line 10 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
Write(Html.ActionLink("Create New", "AddEmployee"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</p>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
#nullable restore
#line 16 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.EId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 19 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.EmpName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 22 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.EmailId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 25 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.MobileNo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 31 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 34 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.EId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 37 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.EmpName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 40 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.EmailId));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 43 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.MobileNo));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 46 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.ActionLink("Edit", "Edit", new {  id=item.EId }));

#line default
#line hidden
#nullable disable
            WriteLiteral(" |\r\n                ");
#nullable restore
#line 47 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.ActionLink("Details", "Details", new { id=item.EId  }));

#line default
#line hidden
#nullable disable
            WriteLiteral(" |\r\n                ");
#nullable restore
#line 48 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
           Write(Html.ActionLink("Delete", "Delete", new { id=item.EId }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 51 "C:\GIT\CRUDOprationAsp.NetCore\CRUDOprationAsp.NetCore\Views\Employee\Index.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<CRUDOprationAsp.NetCore.Models.EmployeeModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
