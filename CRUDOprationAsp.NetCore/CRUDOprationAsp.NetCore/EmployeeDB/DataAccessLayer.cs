﻿using CRUDOprationAsp.NetCore.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CRUDOprationAsp.NetCore.EmployeeDB
{
    public class DataAccessLayer
    {

        // create a connection object with connection string
        //string cs = "Data Source=AVNEESH\\SQLEXPRESS2019;Initial Catalog=MvcDb;User Id=sa;Password=avneesh@123";
        SqlConnection con = new SqlConnection("Data Source=AVNEESH\\SQLEXPRESS2019;Initial Catalog=MvcDb;User Id=sa;Password=avneesh@123;");
        private SqlDataAdapter _adapter;
        private DataSet _ds;


        //public IList<EmployeeModel> GetEmpList()
        //{
        //    IList<EmployeeModel> getEmpList = new List<EmployeeModel>();
        //    _ds = new DataSet();
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("SELECT * FROM EmpTbl", (SqlConnection)con);
        //    con.Close();
        //    cmd.CommandType = CommandType.Text;
        //    _adapter = new SqlDataAdapter(cmd);
        //    _adapter.Fill(_ds);
        //    if (_ds.Tables.Count > 0)
        //    {
        //        for (int i = 0; i < _ds.Tables[0].Rows.Count; i++)
        //        {
        //            EmployeeModel obj = new EmployeeModel();
        //            obj.EId = Convert.ToInt32(_ds.Tables[0].Rows[i]["EId"]);
        //            obj.EmpName = Convert.ToString(_ds.Tables[0].Rows[i]["EmpName"]);
        //            obj.EmailId = Convert.ToString(_ds.Tables[0].Rows[i]["EmailId"]);
        //            obj.MobileNo = Convert.ToString(_ds.Tables[0].Rows[i]["MobileNo"]);
        //            getEmpList.Add(obj);


        //        }
        //    }
        //    return getEmpList;




        //}




        public List<EmployeeModel> GetEmpList()
        {
            DataTable dtblEmployee = new DataTable();
            List<EmployeeModel> lst = new List<EmployeeModel>();
                con.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("SELECT * FROM EmpTbl", con);
            con.Close();
                sqlDa.Fill(dtblEmployee);
                foreach (DataRow dr in dtblEmployee.Rows)
                {
                    lst.Add(new EmployeeModel
                    {
                        EId = Convert.ToInt32(dr[0]),
                        EmpName = Convert.ToString(dr[1]),
                        EmailId = Convert.ToString(dr[2]),
                        MobileNo = Convert.ToString(dr[3]),
                    });
                }
            
            return lst;
        }

        public void InsertEmployee(EmployeeModel model)
        {
            con.Open();

            SqlCommand cmd = new SqlCommand("Insert into EmpTbl values (@EmpName,@EmailId,@MobileNo)", con);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@EmpName", model.EmpName);
            cmd.Parameters.AddWithValue("@EmailId", model.EmailId);
            cmd.Parameters.AddWithValue("@MobileNo", model.MobileNo);


            cmd.ExecuteNonQuery();
            con.Close();

        }
        public EmployeeModel GetEditById(int EId)
        {
            var EmpModelObj = new EmployeeModel();
            con.Open();

            SqlCommand cmd = new SqlCommand("select * from EmpTbl where EId=@EId ", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@EId", EId);
            _adapter = new SqlDataAdapter(cmd);
            _ds = new DataSet();
            _adapter.Fill(_ds);
            if (_ds.Tables.Count > 0 && _ds.Tables[0].Rows.Count > 0)
            {

                EmpModelObj.EId = Convert.ToInt32(_ds.Tables[0].Rows[0]["EId"]);
                EmpModelObj.EmpName = Convert.ToString(_ds.Tables[0].Rows[0]["EmpName"]);
                EmpModelObj.EmailId = Convert.ToString(_ds.Tables[0].Rows[0]["EmailId"]);
                EmpModelObj.MobileNo = Convert.ToString(_ds.Tables[0].Rows[0]["MobileNO"]);
            }
            con.Close();

            return EmpModelObj;
        }
        public void UpdateData(EmployeeModel ur)
        {
        
            SqlCommand sqlcmd = new SqlCommand("UPDATE EmpTbl SET EmpName = @EmpName, EmailId = @EmailId, MobileNo = @MobileNo WHERE EId = @EId ", con);
            sqlcmd.Parameters.AddWithValue("@EId", ur.EId);
            sqlcmd.Parameters.AddWithValue("@EmpName", ur.EmpName);
            sqlcmd.Parameters.AddWithValue("@EmailId", ur.EmailId);
            sqlcmd.Parameters.AddWithValue("@MobileNo", ur.MobileNo);

            con.Open();
            sqlcmd.ExecuteNonQuery();
            con.Close();

        }
        public void DeleteEmployee(int EId)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("DELETE FROM EmpTbl WHERE EId=@EId", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@EId", EId);
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }

}

