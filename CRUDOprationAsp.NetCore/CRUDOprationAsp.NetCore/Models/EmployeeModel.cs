﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace CRUDOprationAsp.NetCore.Models
{
    public class EmployeeModel
    {
        public int EId { get; set; }

        [Required]
        [DisplayName("Employee Name")]
        public string EmpName { get; set; }

        [Required]
        public string EmailId { get; set; }

        [MaxLength(10)]
        public string MobileNo { get; set; }
    }
}
