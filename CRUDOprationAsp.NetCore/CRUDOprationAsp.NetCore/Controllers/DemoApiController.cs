﻿using CRUDOprationAsp.NetCore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CRUDOprationAsp.NetCore.Controllers
{
    public class DemoApiController : Controller
    {
        public IActionResult Index()
        {
            IEnumerable<EmployeeModel> emp = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:61910/weatherforecast");
                var responseTask = client.GetAsync("weatherforecast");//Controller Name
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readJob = result.Content.ReadAsAsync<IList<EmployeeModel>>();

                    readJob.Wait();
                    emp = readJob.Result;
                }
                else
                {
                    //return the error code here
                    emp = Enumerable.Empty<EmployeeModel>();
                    ModelState.AddModelError(string.Empty, "Error occured !");
                }

            }
            return View(emp);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeModel employee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:61910/weatherforecast");
                var postTask = client.PostAsJsonAsync<EmployeeModel>("weatherforecast", employee);
                postTask.Wait();
                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(employee);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            EmployeeModel model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:61910/weatherforecast");
                var responseTask = client.GetAsync("weatherforecast/" + id.ToString());
                responseTask.Wait();
                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<EmployeeModel>();
                    readTask.Wait();
                    model = readTask.Result;

                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EmployeeModel emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:61910/weatherforecast");
                var putTask = client.PutAsJsonAsync<EmployeeModel>("weatherforecast", emp);
                putTask.Wait();
                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
                return View(emp);
            }
        }
        public ActionResult Delete(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:61910/weatherforecast");
                var deleteTask = client.DeleteAsync("weatherforecast/" + id.ToString());
                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                    return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}

