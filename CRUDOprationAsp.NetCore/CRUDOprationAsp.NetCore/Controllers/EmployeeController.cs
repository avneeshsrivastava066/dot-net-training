﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CRUDOprationAsp.NetCore.EmployeeDB;
using CRUDOprationAsp.NetCore.Models;

namespace CRUDOprationAsp.NetCore.Controllers
{
    public class EmployeeController : Controller
    {
        DataAccessLayer dal = new DataAccessLayer();
        public IActionResult Index()
        {
            ModelState.Clear();
            return View(dal.GetEmpList());
        }


        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View(dal.GetEmpList().Find(emp => emp.EId == id));
        }


        public IActionResult AddEmployee()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddEmployee(EmployeeModel model)
        {
            dal.InsertEmployee(model);
            ViewBag.msg = "Data Inserted successfully";
            ModelState.Clear();
            return View();
        }


        public ActionResult Edit(int id)
        {
            return View(dal.GetEmpList().Find(emp => emp.EId == id));
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EmployeeModel ur)
        {
            dal.UpdateData(ur);
            return RedirectToAction("Index");
        }


            //public IActionResult Edit(int Id)
            //{


            //}



            //[HttpPost]
            //public IActionResult Edit(EmployeeModel model)
            //{
            //    dal.UpdateEmployee(model);

            //    var model1 = dal.GetEmpList();

            //    return View("Index", model1);
            //}


            public ActionResult Delete(int id)
        {
            dal.DeleteEmployee(id);
            return RedirectToAction("Index");


        }
    }
}
