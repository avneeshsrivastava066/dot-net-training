﻿using EntityFrameworkProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EntityFrameworkProject.Controllers
{
    public class EmployeeController : Controller
    {
        MvcDbEntities obj = new MvcDbEntities();

        public object EmpTbls { get; private set; }

        // GET: Employee
        public ActionResult Index()
        {
            var x = obj.EmpTbls.ToList();
            return View(x);
        }


        public ActionResult Details(int id)
        {
            EmpTbl Emp = obj.EmpTbls.Find(id);
            if (Emp == null)
            {
                return HttpNotFound();
            }
            return View(Emp);
        }


        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmpTbl Emp)
        {
            if (ModelState.IsValid)
            {
                obj.EmpTbls.Add(Emp);
                obj.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Emp);
        }

        // GET: /Student/Edit/5
        public ActionResult Edit(int id)
        {
            EmpTbl Employee = obj.EmpTbls.Find(id);
            if (Employee == null)
            {
                return HttpNotFound();
            }
            return View(Employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmpTbl emp)
        {
            if (ModelState.IsValid)
            {
                obj.Entry(emp).State = EntityState.Modified;
                obj.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emp);
        }


        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var employee = obj.EmpTbls.Find(id);
            var x = obj.EmpTbls.Where(a => a.EId == id).Select(y => y.EId).FirstOrDefault();

            if(employee==null)
            {
                return View();
            }

            else
            {
                obj.EmpTbls.Remove(employee);
                obj.SaveChanges();
                return RedirectToAction("Index");
            }
           
        }



    }
}